import java.io.{BufferedWriter, File, FileWriter}
import java.util.Scanner

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Runner extends App {

  val fileName = new File(".").getCanonicalPath + "\\data\\c100.txt"
  val outFileName = new File(".").getCanonicalPath + "\\data\\c100f.txt"
  var scanner = new Scanner(new File(fileName))
  val stds: mutable.ListBuffer[Double] = ListBuffer.empty
  while (scanner.hasNext()) {
    scanner.nextLine() // sizes
    val patches: mutable.ListBuffer[Array[Double]] = ListBuffer.empty
    for (_ <- 0 until 8) patches += scanner.nextLine().split("\t").map(_.toDouble)
    val gtPatch = scanner.nextLine().split("\t").map(_.toDouble)
    scanner.nextLine() // weights
    stds += patches.map(patch => patch.zip(gtPatch).map(i => (i._1 + i._2) * (i._1 + i._2)).sum).sum
  }
  scanner.close()
  val size = (0.1 * stds.size).toInt
  // --- choose top 10% patches ---
  val bestStds = stds.zipWithIndex.sortBy(-_._1).take(size)
  val bestStdsIdx = bestStds.map(_._2)

  // --- write patches to file ---
  val bw = new BufferedWriter(new FileWriter(new File(outFileName)))
  scanner = new Scanner(new File(fileName))
  var i = 0
  while (scanner.hasNext()) {
    val write = bestStdsIdx.contains(i)
    for (_ <- 0 until 11) {
      val l = scanner.nextLine()
      if (write) bw.write(l + "\n")
    }
    bw.flush()
    i += 1
  }
  bw.close()
  scanner.close()

}